function toggle(id) {
	el = document.getElementById(id);
	if (el.style.maxHeight == "100ex") {
		el.style.maxHeight = "0";
    	el.style.transition = "max-height 0.25s ease-out";
	}
	else {
		el.style.maxHeight = "100ex";
    	el.style.transition = "max-height 0.25s ease-in";
	}
}

function togglewh(id) {
	el = document.getElementById(id);
	if (el.style.maxWidth == "100ex") {
		el.style.maxWidth = "0";
		el.style.maxHeight = "0";
    	el.style.transition = "max-height, max-width 0.25s ease-out";
	}
	else {
		el.style.maxWidth = "100ex";
		el.style.maxHeight = "100ex";
    	el.style.transition = "max-height, max-width 0.25s ease-in";
	}
}
