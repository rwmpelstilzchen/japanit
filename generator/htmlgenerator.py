#!/usr/bin/env python

import os
import yaml
import argparse
import dominate
from dominate import tags

LESSONS = 1

inputdir = ""
outputdir = ""


def toggle(button, box):
    tags.script("$(\"" + button + "\").click(function() {$(\"" + box + "\").slideToggle(\"slow\");});")


def generate():
    generate_lessons()


def html_head():
    tags.meta(charset='utf-8')
    tags.link(rel="stylesheet", type="text/css", href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css")
    tags.link(rel="stylesheet", type="text/css", href="https://cdn.jsdelivr.net/flexboxgrid/6.2.0/flexboxgrid.min.css")
    tags.link(rel="stylesheet", type="text/css", href="static/fonts/alef/stylesheet.css")
    tags.link(rel="stylesheet", type="text/css", href="static/fonts/japanit-icons/css/japanit-icons.css")
    tags.link(rel="stylesheet", type="text/css", href="static/css/perfect-scrollbar.min.css")
    tags.link(rel="stylesheet", type="text/css", href="static/css/style.css")
    tags.script(type="text/javascript", src="https://code.jquery.com/jquery-1.11.3.min.js")
    tags.script(type="text/javascript", src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js")
    tags.script(type="text/javascript", src="static/js/min/perfect-scrollbar.min.js")
    tags.script(type="text/javascript", src='static/js/toggle.js')


def page_header(right, left, lesson):
    with tags.header(style="justify-content: space-around"):
        with tags.div(cls="row"):
            tags.div(tags.div("<b>יפנית למתחילים</b>", cls="box"), cls="col-xs")
            tags.div(tags.div(tags.button(right + " <span class=\"caret\"></span>", cls="btn btn-default dropdown-toggle", id="tocbtn", style="background-color: #d6b4e6; vertical-align: middle"), style="text-align: center", cls="box"), cls="col-xs")
            toggle("#tocbtn", "#toc")
            tags.div(tags.div("🌸", style="text-align: center", cls="box"), cls="col-xs")
            with tags.div(cls="col-xs", style="text-align: center"):
                with tags.div(cls="dropdown"):
                    tags.button(left + " <span class=\"caret\"></span>", cls="btn btn-default dropdown-toggle", type="button", id="dropdownMenu1", data_toggle="dropdown", aria_haspopup="true", aria_expanded="true", style="background-color: #d6b4e6")
                    with tags.ul(cls="dropdown-menu", aria_labelledby="dropdownMenu1"):
                        tags.li(tags.a("<span class=\"icon-book-open\"></span> טקסט ואוצר־מילים", href=str(lesson)+"-text.html"), style="text-align: right")
                        tags.li(tags.a("<span class=\"icon-cogs\"></span> דקדוק", href=str(lesson)+"-grammar.html"), style="text-align: right")

            tags.div(tags.div("<b>現代日本語テキスト</b>", style="text-align: left", cls="box"), cls="col-xs")
        with tags.div(id="toc", style="display: none"):
            with tags.div(cls="panel panel-default"):
                tags.div(tags.h3("תוכן־עניינים", cls="panel-title"), cls="panel-heading")
                with tags.div(cls="panel-body"):
                    with tags.ul(cls="section"):
                        with tags.li("הקדמות"):
                            with tags.ul(cls="subsection"):
                                tags.li("הקדמה למהדורה השניה")
                                tags.li("הקדמה למהדורה הראשונה")
                                tags.li("שלמי תודה")
                        tags.li("מבוא")
                        with tags.li("שיעורים"):
                            with tags.ul(cls="subsection", style="text-align: justify; display: inline-block; margin-right: 1em"):
                                for lesson in range(LESSONS+39):  # FIXME: +39 for testing purposes only
                                    with tags.li(style="white-space: nowrap; float: right; margin-left: 2em") as lesson_html:
                                        lesson_html += tags.span(str(lesson+1) + " ", style="width: 1.5em; float: right")
                                        lesson_html += "<a href=\"" + str(lesson+1) + "\"><i class=\"icon-book-open\"></i></a>", "<a href=\"" + str(lesson+1) + "\"><i class=\"icon-cogs\"></i></a>"  # FIXME: correct links
                        with tags.li("נספחים"):
                            with tags.ul(cls="subsection"):
                                tags.li("קולופון")
                    tags.span("<b>מקרא</b>: <span class=\"icon-book-open\"></span>=טקסט <span class=\"icon-cogs\"></span>=דקדוק", cls="secondary")


def generate_lessons():
    for lesson in range(1, LESSONS+1):
        generate_lesson(lesson)


def generate_lesson(lesson):
    doc = dominate.document(title='יפנית למתחילים: שיעור')
    with doc.head:
        html_head()
    with doc.body:
        page_header("שיעור 1", "<i class=\"icon-book-open secondary\"></i> טקסט ואוצר־מילים", lesson=1)
        with tags.div(cls="main"):
            # resize: vertical
            with tags.div(cls="pscont", style="height: 45vh"):
                generate_lesson_texts(lesson)
            tags.hr(cls="hsplit")
            with tags.div(cls="pscont", style="height: 40vh; width: 45%; float: right"):
                generate_lesson_wordlist(lesson)
            with tags.div(cls="pscont", style="height: 40vh; width: 45%; float: left"):
                generate_lesson_kanzi(lesson)
            tags.script("""
              window.onload = function () {
                [].forEach.call(document.querySelectorAll('.pscont'), function (el) {
                  Ps.initialize(el);
                });
              };

            $("input:checkbox").click(function(){var column = "table ." + $(this).attr("name"); $(column).fadeToggle();});
            """)
        # with tags.footer(style="justify-content: space-around"):
        #     with tags.ul(cls="flex-container", style="justify-content: space-around"):
        #         tags.li("<strong>יפנית למתחילים</strong>, מאת מריקו וקיוג׳י צוג׳יטה")
    print(dominate.util.unescape(str(doc)))


def generate_lesson_texts(lesson):
    yamlfilename = os.path.join(inputdir, 'lessons/texts/') + '{0:02d}'.format(lesson) + '.yaml'
    with open(yamlfilename) as f:
        texts = yaml.load_all(f)
        for text in texts:
            generate_lesson_text(text)


def generate_lesson_text(text):
    texttable = tags.table(style="direction: ltr; width: 100%", id="text")
    with texttable:
        toggleall = ""
        for id in range(len(text['dialogue'])):
            toggleall += "toggle('id-" + text['header']['j'] + "-" + str(id) + "a'); \
                toggle('id-" + text['header']['j'] + "-" + str(id) + "b'); "
            with tags.tr():
                tags.th("⋄", onclick=toggleall)
                tags.th(text['header']['j'], colspan='2')
                tags.th(text['header']['h'], colspan='2', cls="heb-"+text['header']['j'])
                id = 0
                for speech in text['dialogue']:  # TODO: rewrite better, in a general manner
                    with tags.tr():  # Japanese and Hebrew
                        tags.td("&nbsp;")
                        tags.td(speech['interlocutor']['j'],
                                onclick="toggle('id-" + text['header']['j'] + "-" + str(id) + "a'); \
                                toggle('id-" + text['header']['j'] + "-" + str(id) + "b')",
                                cls="interlocutorltr")
                        tags.td(speech['content']['j'],
                                onclick="toggle('id-" + text['header']['j'] + "-" + str(id) + "a'); \
                                toggle('id-" + text['header']['j'] + "-" + str(id) + "b')")
                        tags.td(speech['content']['h'], style="direction: rtl", cls="heb-"+text['header']['j'])
                        tags.td(speech['interlocutor']['h'],
                                cls="interlocutorrtl heb-"+text['header']['j'])
                        with tags.tr(cls="kanatext"):  # Transcription
                            tags.td()
                            tags.td(tags.div(speech['interlocutor']['r'],
                                             cls="kanainterlocutorltr hiddenobjh",
                                             id="id-" + text['header']['j'] + "-" + str(id) + "a"), valign="top")
                            tags.td(tags.div(speech['content']['r'],
                                             cls="hiddenobjh",
                                             id="id-" + text['header']['j'] + "-" + str(id) + "b"))
                            id += 1
                            tags.label(tags.input(type="checkbox", name="heb-"+text['header']['j'], checked="checked"), " עברית")


def generate_lesson_wordlist(lesson):
    yamlfilename = os.path.join(inputdir, 'lessons/wordlists/') + '{0:02d}'.format(lesson) + '.yaml'
    with open(yamlfilename) as f:
        wordlists = yaml.load_all(f)
        for wordlist in wordlists:
            with tags.table(style="direction: ltr"):
                for word in wordlist:
                    with tags.tr():
                        tags.td(word['word'], style="direction: ltr")
                        tags.td(word['translation'], style="direction: rtl")
                        tags.td(word['pos'], cls="wordlistpos")


def generate_lesson_kanzi(lesson):
    yamlfilename = os.path.join(inputdir, 'lessons/kanzi/') + '{0:02d}'.format(lesson) + '.yaml'
    with open(yamlfilename) as f:
        kanzilists = yaml.load_all(f)
        for kanzis in kanzilists:
            i = 1
            with tags.div(id="kanzi"):
                with tags.table():
                    for kanzi in kanzis:
                        with tags.tr(onclick="togglewh('id-" + kanzi['kanzi'] + "')"):
                            tags.td(i)
                            tags.td(kanzi['kanzi'], cls="kanzi")
                            # tags.img(src = "static/strokes-serial/" + ("0x%0.5X" % ord(kanzi['kanzi']))[2:].lower() + "-strokes.svg")
                            tags.td(
                                tags.div(tags.img(src="static/strokes/" + kanzi['kanzi'] + ".svg",
                                                  style="width: 5em"),
                                         cls="hiddenobjwh",
                                         id="id-" + kanzi['kanzi']))
                            with tags.td(style="text-align: center"):
                                for x in kanzi['context']:
                                    with tags.ruby(x['j']):
                                        if x['k']:
                                            if 's' in x and x['s']:
                                                tags.rt(tags.u(x['k']))
                                            else:
                                                tags.rt(x['k'])
                            tags.td(kanzi['h'], style="direction: rtl")
                        i += 1


def main():
    parser = argparse.ArgumentParser(description="Generator for Tuzita’s Japanese Textbook.")
    parser.add_argument("-i", "--inputdir", type=str,
                        help="input directory")
    parser.add_argument("-o", "--outputdir", type=str,
                        help="output directory")
    args = parser.parse_args()
    global inputdir
    global outputdir
    inputdir = args.inputdir
    outputdir = args.outputdir
    generate()

if __name__ == "__main__":
    main()
